Source: libflorist
Section: libdevel
Priority: optional
Maintainer: Ludovic Brenta <lbrenta@debian.org>
Uploaders: Nicolas Boulenguez <nicolas@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-ada-library (>= 9.1),
 dh-sequence-ada-library,
 gnat,
 gnat-14,
 gprbuild (>= 2025.0.0-2)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/libflorist
Vcs-Git: https://salsa.debian.org/debian/libflorist.git
Homepage: https://github.com/AdaCore/florist

Package: libflorist-dev
Breaks: libflorist2018-dev, libflorist2019-dev, libflorist2020-dev,
 libflorist2021-dev, libflorist2022-dev
Replaces: libflorist2018-dev, libflorist2019-dev, libflorist2020-dev,
 libflorist2021-dev, libflorist2022-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends},
 libnsl-dev,
Description: POSIX.5 Ada interface to operating system services: development
 FLORIST is an implementation of the IEEE Standards 1003.5: 1992, IEEE
 STD 1003.5b: 1996, and parts of IEEE STD 1003.5c: 1998, also known as
 the POSIX Ada Bindings.  Using this library, you can call operating
 system services from within Ada programs.
 .
 No one seems to maintain this library anymore.
 For new packages, please consider other solutions.
 .
 This package contains the development files and unstripped static
 library.

Package: libflorist2021.1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: POSIX.5 Ada interface to operating system services: shared library
 FLORIST is an implementation of the IEEE Standards 1003.5: 1992, IEEE
 STD 1003.5b: 1996, and parts of IEEE STD 1003.5c: 1998, also known as
 the POSIX Ada Bindings.  Using this library, you can call operating
 system services from within Ada programs.
 .
 No one seems to maintain this library anymore.
 For new packages, please consider other solutions.
 .
 This package contains the run-time shared library.
